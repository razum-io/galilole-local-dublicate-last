<div id="instractor" class="art-Post">
    <div class="art-Post-tl"></div>
    <div class="art-Post-tr"></div>
    <div class="art-Post-bl"></div>
    <div class="art-Post-br"></div>
    <div class="art-Post-tc"></div>
    <div class="art-Post-bc"></div>
    <div class="art-Post-cl"></div>
    <div class="art-Post-cr"></div>
    <div class="art-Post-cc"></div>
    <div class="art-Post-body">
<? if ($teaser) { ?>
      <div class="art-Post-inner teaser">
        <h2 class="art-PostHeader"> <?php echo art_node_title_output($title, $node_url, $page); ?></h2>
        <div id="instructorBody"><?php print $node->content['body']['#value'] ?></div>
        <div id="instructorPhoto">
          <?php 
            if ($node->field_instructor_photo[0]['view'] != ''){
              print $node->field_instructor_photo[0]['view'];
            }else {
              echo '&nbsp;';
            }
            
          ?>
        </div>
<? 

if ($node->field_sadna_link[0]['view']!='' )      { ?>
        <div id="instructorLink">קישור לסדנאות:<br> 
        <?php 
          foreach ($node->field_sadna_link as $link){
          print $link['view'] .'<br>'; 
          }
          ?>
        </div>
<? } ?>        
        <?php if (isset($node->links['node_read_more'])) { echo '<div class="read_more">'.get_html_link_output($node->links['node_read_more']).'</div>'; }?>
<? }else{?>
     <div id="instructorPhoto"><?php print $node->field_instructor_photo[0]['view'];?></div>
     <div id="instructorLogo"><?= $node->field_instructor_logo[0]['view'] ?></div>
     <div style="clear:both;"></div>
     <h2 class="art-PostHeader"><?php echo art_node_title_output($title, $node_url, $page); ?></h2>
     <div id="instructorDeitails">
<? if ($node->field_biz_name[0]['view'] != ''){ ?>        
       <b><?= $node->content['field_biz_name']['field']['#title']; ?></b>: <?=$node->field_biz_name[0]['view'];  ?><br>
<? } ?> 
       
<? if ($node->field_prof[0]['view'] != ''){ ?>       
       <b><?= $node->content['field_prof']['field']['#title']; ?></b>: <?=$node->field_prof[0]['view'];  ?><br>
<? } ?>       
  
<? if ($node->field_num_years_in_biz[0]['view'] != ''){ ?>       
       <b><?= $node->content['field_num_years_in_biz']['field']['#title']; ?></b>: <?=$node->field_num_years_in_biz[0]['view'];  ?><br>
<? } ?>       
       <div id="instructorLinks">
       <? if ($node->field_website_link[0]['view'] != ''){ ?>       
        <?=$node->field_website_link[0]['view'];  ?><br>
<? } ?>   
     </div>
     <div id="instructorBody"><?=$node->content['body']['#value'] ;?></div>
     <? //print_r($node->content->field_biz_name); ?>
     <? if ($node->field_sadna_link[0]['view'] != ''){ ?>       
       <b><?= $node->content['field_sadna_link']['field']['#title']; ?></b>:<br> 
       <? foreach ($node->field_sadna_link as $link2sadna){  ?>
         <?= $link2sadna['view'];?>
       <br>
       <? } ?>
       <br><br>
     
        <div style="height:69px;">&nbsp;</div>
<? } ?>    
     </div>
  
<? }?>

<div class="cleared"></div>

</div>

    </div>
</div>
