<div class="art-Post smaller">
    <div class="art-Post-tl"></div>
    <div class="art-Post-tr"></div>
    <div class="art-Post-bl"></div>
    <div class="art-Post-br"></div>
    <div class="art-Post-tc"></div>
    <div class="art-Post-bc"></div>
    <div class="art-Post-cl"></div>
    <div class="art-Post-cr"></div>
    <div class="art-Post-cc"></div>
    <div class="art-Post-body">
<div class="art-Post-inner">
<h2 class="art-PostHeader"> <?php echo art_node_title_output($title, $node_url, $page); ?>
</h2>
<? if (!empty($terms)) { ?>
	<div class="taxonomy"><b>תגיות:</b> <?php print $terms?></div>
<? } ?>

<div class="art-PostContent smaller">
<div class="art-article"><?php print $picture; ?><?php echo $content; ?>
<?php if (isset($node->links['node_read_more'])) { echo '<div class="read_more">'.get_html_link_output($node->links['node_read_more']).'</div>'; }?></div>
</div>
<div class="cleared"></div>
<div><?= $links ?></div>

</div>

    </div>
</div>
 