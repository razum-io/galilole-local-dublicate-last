<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php echo get_page_language($language); ?>" xml:lang="<?php echo get_page_language($language); ?>">

<head>



  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" /> 
    <!--[if IE]>
		<meta http-equiv="Page-Enter" content="blendTrans(duration=0)" />
		<meta http-equiv="Page-Exit" content="blendTrans(duration=0)" />
	<![endif]-->
  
  <?php echo $head; ?>
  <title><?php if (isset($head_title )) { echo $head_title; } ?></title>  
  <?php echo $styles ?>
  <!--[if lt IE 9]><link rel="stylesheet" href="http://cdn3.easysitenow.net/sites/all/themes/galiole/ie-fix.css" type="text/css" media="screen" />
	<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
	<![endif]--> 
  <!-- <script src="http://cdn4.easysitenow.net/sites/all/modules/jquery_update/replace/jquery.min.js?0" type="text/javascript"></script>
   <script src="http://cdn4.easysitenow.net/misc/drupal.js?0" type="text/javascript"></script>-->
 <?php echo $scripts ?>

</head>

<body class="home"> 
  <?php if (!empty($admin)) print $admin; ?>
   <div class="wrapper">
   			
  		   <?php if(!empty($leftFloatingAd)){?>
		 	  	<div id="leftFloatingAd"><?= $leftFloatingAd;?></div>
		   <?}?> 
		   
		   <?php if(!empty($rightFloatingAd)){?>
		 	  	<div id="rightFloatingAd"><?= $rightFloatingAd;?></div>
		   <?}?>

		   <div class="container">
   				<?php if (!empty($topNavigation)) {?>
				<?php	echo '<div id="topNavigation" class="panel both">'.$topNavigation; ?>
				<?php  if (!empty($search_box)): ?>
		     	   <div id="search-box" class="art-Block clear-block block block-block"><?php print $search_box; ?></div>
		        <?php endif; ?>
				<?php echo '</div>'; } ?>
				
				<div class="panel-topB panel both"></div>
				<?php if (!empty($navigation)) { echo '<div id="navigation" class="panel both block-menu"><div class="inner-menu">'.$navigation.'</div></div>'; } ?>
			
				<?php if (!empty($banner1)) { echo '<div id="banner1" >'.$banner1.'</div>'; } ?>
				<div class="cleared" ></div>
			
				

<?php //echo art_placeholders_output($top1, $top2, $top3); ?>

		<div class="<?php echo (!empty($left) || !empty($sidebar_left)) ? 'panel-mainL panel left' : 'art-content-wide'; ?>">
			<?php if (!empty($banner2)) { echo '<div id="banner2">'.$banner2.'</div>'; } ?>
			<?php if ((!empty($user1)) && (!empty($user2))) : ?>
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
			<tr valign="top"><td width="50%"><?php echo $user1; ?></td>
			<td><?php echo $user2; ?></td></tr>
			</table>
			<?php else: ?>
			<?php if (!empty($user1)) { echo '<div id="user1">'.$user1.'</div>'; }?>
			<?php if (!empty($user2)) { echo '<div id="user2">'.$user2.'</div>'; }?>
			<?php endif; ?>
			
			
			
			<?php // if (($is_front) || (isset($node) && isset($node->nid))): ?> 
			<?php if (true): ?> 
			             
				<?php if (!empty($breadcrumb) || !empty($tabs) || !empty($tabs2)): ?>
				
					<div class="art-PostContent">
						<?php if (!empty($breadcrumb)) { echo $breadcrumb; } ?>
						<?php if (!empty($tabs)) { echo $tabs.'<div class="cleared"></div>'; }; ?>
						<?php if (!empty($tabs2)) { echo $tabs2.'<div class="cleared"></div>'; } ?>
					</div>
					
				<?php endif; ?>

				<div class="cleared"></div>
			

			
			<?php if (!empty($mission)) { echo '<div id="mission">'.$mission.'</div>'; }; ?>
			<?php if (!empty($help)) { echo $help; } ?>
			<?php if (!empty($messages)) { echo $messages; } ?>
			<?php //echo $content; ?>
			
			<?php endif; ?>

		</div>	
		
		<?php if (!empty($left)) echo '<div class="panel-mainR" id="left">' . $left . "</div>";?>
		<div class="panel-BottomTop panel right">
			<?php if (!empty($banner4)) { echo '<div id="banner4">'.$banner4.'</div>'; } ?> 
		</div>
		<div class="panel left panel-BottomCright">
			<?php if (!empty($banner3)) { echo '<div id="banner3">'.$banner3.'</div>'; } ?>
		</div>
		<div class="panel-BottomRight panel right">
			<?php if (!empty($banner5)) { echo '<div id="banner5">'.$banner5.'</div>'; } ?>
		</div>
		<div class="panel-Bottomleft panel left">
			<?php if (!empty($right)) { echo '<div id="banner6">'.$right.'</div>'; } ?>
		</div>
		
				
		
	
		
		
		
		<div class="panel-BottomCleft panel">
			<?php if (!empty($banner7)) { echo '<div id="banner7">'.$banner7.'</div>'; } ?>
		</div>

<?php if (!empty($user3) && !empty($user4)) : ?>
<table width="100%" cellpadding="0" cellspacing="0" border="0">
<tr valign="top"><td width="50%"><?php echo $user3; ?></td>
<td><?php echo $user4; ?></td></tr>
</table>
<?php else: ?>
<?php if (!empty($user3)) { echo '<div id="user1">'.$user3.'</div>'; }?>
<?php if (!empty($user4)) { echo '<div id="user2">'.$user4.'</div>'; }?>
<?php endif; ?>





<div class="cleared"></div>


<?php echo art_placeholders_output($bottom1, $bottom2, $bottom3); ?>
<?php if (!empty($bottom4)) { echo '<div id="banner6">'.$bottom4.'</div>'; } ?>

 <?php if (!empty($copyright) && !empty($footer_message)) { ?>
<div id="footerBlock" class="footer">
	<div class="footerMap">
		 <?php if (!empty($copyright)) { echo $copyright; } ?>
		 <div class="fotter-extraLinks"> 
			<?php if (!empty($extraLinks) && (trim($extraLinks) != '')) {  
		        echo $extraLinks;
		     }?>
		</div>
	</div>
	<div class="footer-message">
		 <?php if (!empty($footer_message) && (trim($footer_message) != '')) {  
	        echo $footer_message;
	     }?>
	</div>
</div>
 <?php } ?>
 

<div class="cleared"></div>

</div></div>

<script src="http://cdn4.easysitenow.net/sites/all/themes/galiole/script.js?g" type="text/javascript"></script>
<script src="http://www.google-analytics.com/urchin.js" type="text/javascript"></script>
<script type="text/javascript">
	_uacct = "UA-635031-6";
	urchinTracker();
</script>

<?php print $closure; ?>

</body>
</html>