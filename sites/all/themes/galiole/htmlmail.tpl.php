<html>
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   <style type="text/css" media="screen">
      
      a {
        color:#386690
      }

      h1, h2, h3, h4, h5, h6 {
      color:#f4793a;
      }

      table.htmlmail-main {
         background-color: #ffffff;
      }

      td.htmlmail-header {
       
         font-family:Arial, Helvetica, sans-serif;
         color: #ffffff;
         font-size: 14px;
         padding: 0;
      }

      td.htmlmail-header a,
      td.htmlmail-footer a {
         color: #aebac5;
      }

      td.htmlmail-body {
         font-family:Arial, Helvetica, sans-serif;
         font-size: 12px;
         padding: 10px 20px 10px 20px;
         background-color: #ffffff;
         direction:rtl;
      }

      td.htmlmail-footer {
         font-family:Arial, Helvetica, sans-serif;
         font-size: 11px;
         color: #ffffff;
         line-height: 16px;
         padding: 10px 20px 10px 20px;
      
         vertical-align: middle;
      }
   <?php print $css; ?>
   

   </style>
   
</head>

<body dir="rtl">
  <table width="800" dir="rtl" border="0" cellspacing="0" cellpadding="0" class="htmlmail-background">
    <tr>
      <td align="center">
        <table width="800" border="0" cellspacing="0" cellpadding="0" class="htmlmail-main">
          
          <?php if ($header): ?>
          <tr>
            <td valign="top" align="right" class="htmlmail-header">
              <?php print $header; ?>
            </td>
          </tr>
          <?php endif; ?>

          <tr>
            <td valign="top" align="right" class="htmlmail-body">
              <?php print $body; ?>

            </td>
          </tr>

          <tr>
            <td valign="middle" align="right" class="htmlmail-footer">
              <?php print $footer; ?>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</body>
</html>
