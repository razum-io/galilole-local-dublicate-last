<div class="art-Post">
    <div class="art-Post-tl"></div>
    <div class="art-Post-tr"></div>
    <div class="art-Post-bl"></div>
    <div class="art-Post-br"></div>
    <div class="art-Post-tc"></div>
    <div class="art-Post-bc"></div>
    <div class="art-Post-cl"></div>
    <div class="art-Post-cr"></div>
    <div class="art-Post-cc"></div>
    <div class="art-Post-body">
<div class="art-Post-inner">
<h2 class="art-PostHeader"> <?php echo art_node_title_output($title, $node_url, $page); ?>
</h2>
<? if (!empty($terms)) { ?>
	<div class="taxonomy"><b>תגיות:</b> <?php print $terms?></div>
<? } ?>

<div class="art-PostContent">
<div class="art-article"><?php print $picture; ?><?php echo $content; ?>
<?php $contrib = strip_tags($node->field_contribution[0]['value']);  if (strlen($contrib) > 2) { echo '<div class="contribution"><b>תרומה לספורטאי הצפון:</b>  ' . $contrib .'</div>'; }?></div>

</div>
<div class="cleared"></div>
<div><?= $links ?></div>

</div>

    </div>
</div>
