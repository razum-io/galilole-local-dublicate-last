
<?php $nodeid = $node->nid; ?>
<?php $group = true; ?>

<?php if(strstr($body_classes, 'wide') != false){
	$group =  false;
}elseif(strstr($body_classes, 'node-type-organic-group') != false){
	$group =  false;
}elseif(strstr($body_classes, 'node-type-adowner') != false){
	$group =  false;
}
?>
<?php
function getBrowser()
{
    $u_agent = $_SERVER['HTTP_USER_AGENT'];
    $bname = 'Unknown';
    $platform = 'Unknown';
    $version= "";

    //First get the platform?
    if (preg_match('/linux/i', $u_agent)) {
        $platform = 'linux';
    }
    elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
        $platform = 'mac';
    }
    elseif (preg_match('/windows|win32/i', $u_agent)) {
        $platform = 'windows';
    }
   
    // Next get the name of the useragent yes seperately and for good reason
    if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent))
    {
        $bname = 'Internet Explorer';
        $ub = "MSIE";
    }
    elseif(preg_match('/Firefox/i',$u_agent))
    {
        $bname = 'Mozilla Firefox';
        $ub = "Firefox";
    }
    elseif(preg_match('/Chrome/i',$u_agent))
    {
        $bname = 'Google Chrome';
        $ub = "Chrome";
    }
    elseif(preg_match('/Safari/i',$u_agent))
    {
        $bname = 'Apple Safari';
        $ub = "Safari";
    }
    elseif(preg_match('/Opera/i',$u_agent))
    {
        $bname = 'Opera';
        $ub = "Opera";
    }
    elseif(preg_match('/Netscape/i',$u_agent))
    {
        $bname = 'Netscape';
        $ub = "Netscape";
    }
   
    // finally get the correct version number
    $known = array('Version', $ub, 'other');
    $pattern = '#(?<browser>' . join('|', $known) .
    ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
    if (!preg_match_all($pattern, $u_agent, $matches)) {
        // we have no matching number just continue
    }
   
    // see how many we have
    $i = count($matches['browser']);
    if ($i != 1) {
        //we will have two since we are not using 'other' argument yet
        //see if version is before or after the name
        if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
            $version= $matches['version'][0];
        }
        else {
            $version= $matches['version'][1];
        }
    }
    else {
        $version= $matches['version'][0];
    }
   
    // check if we have a number
    if ($version==null || $version=="") {$version="?";}
   
    return array(
        'userAgent' => $u_agent,
        'name'      => $bname,
        'version'   => $version,
        'platform'  => $platform,
        'pattern'    => $pattern
    );
}

// now try it
$ua=getBrowser();
$yourbrowser= $ua['name'];
if($yourbrowser == 'Apple Safari') { ?>
 
<?php }
?>
<body class="<?php print $body_classes; ?>" id="node-<?php echo $nodeid; ?>">
	<!-- highlighted -->
	<div id="leftFloatingAd"><?php print render($page['highlighted']); ?></div>
	<!-- highlighted end-->
	<?php if (!empty($admin)) print $admin; ?>
	<div class="wrapper">
		<div class="container">
			
		<!--advertisments start-->
			<div id="leftFloatingAd"><?php print render($page['leftFloatingAd']); ?></div>
			<div id="rightFloatingAd"><?php print render($page['rightFloatingAd']); ?> </div>
		<!--advertisments end-->

		<!-- mylogo -->
		<div class="cleared" ></div>
			<div id="logo">
				 <?php print render($page['mylogo']); ?>
			</div>
			<div class="cleared" ></div>
		<!-- mylogo -->

		    <!-- topMenu -->
		<div id="topMenu">
		<?php print render($page['topMenu']); ?>
		</div>
			<!-- topMenu end-->
	       

		    <!-- mainMenu -->
		<div id="main-menu">
		<?php print render($page['mainMenu']); ?>
		</div>
			<!-- mainMenu end-->
				

			<!-- mainAd -->
		<div id="mainAd">
		<?php print render($page['mainAd']); ?>
		</div>
			<!-- mainAd end-->


			<!-- rightNavigation -->
		<div id="rightNavigation">
		<?php print render($page['rightNavigation']); ?>
		</div>
			<!-- rightNavigation end-->

		    <!-- breadcrumbTabs -->
		    <div id="breadcrumbTabs" >
			   	<?php print render($page['help']); ?>
			   	<?php print render($page['messages']); ?>
			   	<?php print render($page['breadcrumb']); ?>
			   	<?php print render($page['tabs']); ?>
			   	<?php print render($page['tabs2']); ?>
			   	<?php print render($page['breadcrumbTabs']); ?>
		    </div>
			<!-- breadcrumbTabs end -->

 			<!-- mainContent -->
		    <div id="mainContent">
		   		<?php print render($page['content']); ?>
		   </div>
		    <!-- mainContent end  -->

		    <!-- leftInner -->
		   <? if($group){ ?>
		   <div id="leftInner">
		   		<?php print render($page['leftInner']); ?>
		   </div>
		   <? } ?>
		   <!-- leftInner end -->

		   <div class="cleared" ></div>
		    <!-- footer -->
		   <div id="footerArea">
		   		<div id="footer">
		   		<?php print render($page['footer']); ?>
		   		</div>
		   		<div id="bottomMessage">
		   		<?php print render($page['footer_message']); ?>
		   </div>
		    <!-- footer end -->
		   <div class="cleared" ></div>
		</div>
	</div>
<?php echo $scripts ?>
<script src="/sites/all/themes/galilolel/scripts/admin.js"></script>

<script type="text/javascript">
  var _gaq = _gaq || [];
//  _gaq.push(['_setAccount', 'UA-25695376-1']);
  _gaq.push(['_setAccount', 'UA-41413092-1']);
  _gaq.push(['_setDomainName', 'galilole.org.il']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-68135664-1', 'auto');
  ga('send', 'pageview');

</script>

<?php print $closure; ?>

</body>