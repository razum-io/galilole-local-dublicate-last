<body class="advertisers-body">
<?php print $page_top; ?>
<?php if (!empty($admin)) print $admin; ?>
	<div class="wrapper">
		<div class="container">
			<?php if (!empty($rightNavigation)) { echo '<div id="rightNavigation" >'.$rightNavigation.'</div>'; } ?>
		    
		    <div id="breadcrumbTabs" >
			    <?php if (!empty($help)) { echo $help; } ?>
				<?php if (!empty($messages)) { echo $messages; } ?>
		 	    <?php if (!empty($breadcrumb)) { echo $breadcrumb; } ?>
				<?php if (!empty($tabs)) { echo $tabs.'<div class="cleared"></div>'; }; ?>
				<?php if (!empty($tabs2)) { echo $tabs2.'<div class="cleared"></div>'; } ?>
		  		<?php if (!empty($breadcrumbTabs)) { echo $breadcrumbTabs; } ?>
		    </div>
		    <div id="mainContent">
		   		<?php print render($content); ?>
		   </div>
		   <div class="cleared" ></div>
		</div>
	</div>
	<!--[if lt IE 9]>
		<script src="http://cdn4.easysitenow.net/sites/all/themes/galilolel/roundIE.js"></script>
	<![endif]--> 
<?php echo $scripts ?>

<script src="http://cdn4.easysitenow.net/sites/all/themes/galilolel/script.js"></script>
<script src="http://www.google-analytics.com/urchin.js" type="text/javascript"></script>
<script type="text/javascript">
	_uacct = "UA-635031-6";
	urchinTracker();
</script>
<?php print $page_bottom; ?>

</body>