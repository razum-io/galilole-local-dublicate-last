
<?php $nodeid = $node->nid; ?>
<?php $group = true; ?>

<?php if(strstr($body_classes, 'wide') != false){
	$group =  false;
}elseif(strstr($body_classes, 'node-type-organic-group') != false){
	$group =  false;
}elseif(strstr($body_classes, 'node-type-adowner') != false){
	$group =  false;
}
?>
<body class="<?php print $body_classes; ?>" id="node-<?php echo $nodeid; ?>">
<?php print $page_top; ?>
<?php if (!empty($admin)) print $admin; ?>
	<div class="wrapper">
		<div class="container">
			
		<!--advertisments start-->
			<?php if(!empty($leftFloatingAd)){?><div id="leftFloatingAd"><?= $leftFloatingAd;?></div><?}?> 
		    <?php if(!empty($rightFloatingAd)){?><div id="rightFloatingAd"><?= $rightFloatingAd;?></div><?}?>
		<!--advertisments end-->
		
		    <?php if (!empty($topMenu)) {?>
			<?php	echo '<div id="topMenu">'.$topMenu; ?>
			
	        <?php echo '</div>'; } ?>
	       
			
			<div class="cleared" ></div>
			<?php if (!empty($mainMenu)) { echo '<div id="mainMenu">'.$mainMenu.'</div>'; } ?>
			<div class="cleared" ></div>
			<?php if (!empty($mainAd)) { echo '<div id="mainAd" >'.$mainAd.'</div>'; } ?>
			<div class="cleared" ></div>
		    <?php if (!empty($rightNavigation)) { echo '<div id="rightNavigation" >'.$rightNavigation.'</div>'; } ?>
		    
		    <div id="breadcrumbTabs" >
			    <?php if (!empty($help)) { echo $help; } ?>
				<?php if (!empty($messages)) { echo $messages; } ?>
		 	    <?php if (!empty($breadcrumb)) { echo $breadcrumb; } ?>
				<?php if (!empty($tabs)) { echo $tabs.'<div class="cleared"></div>'; }; ?>
				<?php if (!empty($tabs2)) { echo $tabs2.'<div class="cleared"></div>'; } ?>
		  		<?php if (!empty($breadcrumbTabs)) { echo $breadcrumbTabs; } ?>
		    </div>
		    <div id="mainContent">
		   		<?php print render($content); ?>
		   </div>
		   <? if($group){ ?>
		   <div id="leftInner">
		   		<?php echo $leftInner; ?>
		   </div>
		   <? } ?>
		   <div class="cleared" ></div>
		   
		</div>
	</div>
<?php echo $scripts ?>
<script src="http://cdn4.galilole.org.il/sites/all/themes/galilolel/scripts/admin.js"></script>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-25695376-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-68135664-1', 'auto');
  ga('send', 'pageview');

</script>
<?php print $page_bottom; ?>

</body>